import api from './api';

export default {
    getImages(per_page) {
        return api().get('/photos', {
            params: {
                per_page
            }
        })
    },
    getImage(id) {
        return api().get(`/photos/${id}`)
    },
    loadMore(page) {
        return api().get('/photos', {
            params: {
                page,
                per_page: 6
            }
        })
    }
}