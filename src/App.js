import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import Header from './components/header/Header';
import Switcher from './components/Switcher';

import './App.scss';

import store from './store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Router>
            <Header />
            <Route component={Switcher} />
          </Router>
        </div>
      </Provider>
    );
  }
}

export default App;
