import { FETCH_IMAGES, LOAD_IMAGES, GET_IMAGE } from './types';
import api from '../services/unsplash';

export const fetchImages = () => async dispach => {
    const { data } = await api.getImages(12);
    dispach({
        type: FETCH_IMAGES,
        payload: data
    })
}

export const getImage = (id) => async dispach => {
    const { data } = await api.getImage(id);
    dispach({
        type: GET_IMAGE,
        payload: data
    })
}

export const loadMore = (page) => async dispach => {
    const { data } = await api.loadMore(page);
    dispach({
        type: LOAD_IMAGES,
        payload: data
    })
}