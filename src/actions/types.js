export const FETCH_IMAGES = 'FETCH_IMAGES';
export const LOAD_IMAGES = 'LOAD_IMAGES';
export const GET_IMAGE = 'GET_IMAGE';
