import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"; // eslint-disable-line
import Gallery from "./gallery/Gallery";
import ImageView from './image-view/ImageView';
import Modal from './pop-up/PopUp';
import About from './about/About';

class Switcher extends Component {

    previousLocation = this.props.location;
  
    componentWillUpdate(nextProps) {
      let { location } = this.props;
  
      if (
        nextProps.history.action !== "POP" &&
        (!location.state || !location.state.modal)
      ) {
        this.previousLocation = this.props.location;
      }
    }
  
    render() {
      let { location } = this.props;
  
      let isModal = !!(
        location.state &&
        location.state.modal &&
        this.previousLocation !== location &&
        window.innerWidth > 992
      );
      
      return (
        <div>
          <Switch location={isModal ? this.previousLocation : location}>
            <Route exact path="/" component={Gallery} />
            <Route path="/p/:id" component={ImageView} />
            <Route path="about" component={About} />
          </Switch>
          {isModal ? <Route path="/p/:id" component={Modal} /> : null}
        </div>
      );
    }
  }

export default Switcher;