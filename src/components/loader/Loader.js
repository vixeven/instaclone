import React, { Component } from "react";

import './Loader.scss';

class Loader extends Component {
  render() {
    return (
        <div className="loading">
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
  }
}

export default Loader;
