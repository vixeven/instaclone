import React, { Component } from "react";
import Avatar from 'antd/lib/avatar'

import logo from './logo.png'
import './Profile.scss';

class Profile extends Component {
  render() {
    return (
        <div className="profile">
            <Avatar src={logo} />
            <div className="info">
                <span className="username">ebsintegrator</span>
                <div className="statistic">
                    <span>
                        <span className="count">48</span> posts
                    </span>
                    <span>
                        <span className="count">113</span> followers
                    </span>
                    <span>
                        <span className="count">130</span> following
                    </span>
                </div>
                <span>
                    Followed by <a href='https://instagram.com/neluarseni'>neluarseni</a>, <a href='https://instagram.com/kiselev_grigor'>kiselev_grigor</a> + 16 more
                </span>
            </div>
        </div>
    );
  }
}

export default Profile;
