import React, { Component } from 'react';
import { BrowserRouter as Link } from "react-router-dom"; // eslint-disable-line

import Image from '../image/Image';
import Modal from 'antd/es/modal';
import PostInfo from '../post-info/PostInfo';

import PropTypes from 'prop-types'
import { connect } from 'react-redux';

import './PopUp.scss'

class PopUp extends Component {
  render() {
    
    let image = this.props.images.find(image => image.id === this.props.match.params.id);

    if (!image) return null;
    
    let back = e => {
      e.stopPropagation();
      this.props.history.goBack();
    };

    return (
      <Modal
        centered
        className='popUp'
        width='auto'
        visible={true}
        footer={null}
        onCancel={back}
      >
          <Image url={image.urls.regular}/>
          <PostInfo image={image} />
      </Modal>
    );
  }
}

PopUp.propTypes = {
  images: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
  images: state.images.items
})

export default connect(mapStateToProps)(PopUp);
