import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';

import { connect } from 'react-redux';
import { fetchImages, loadMore } from '../../actions/imageActions';


import Profile from "../profile/Profile";
import Layout from "antd/lib/layout";

import './Gallery.scss';
import Loader from "../loader/Loader";
import Thumbnail from "../thumbnail/Thumbnail";

const { Content } = Layout;


class Gallery extends Component {

  componentWillMount() {
    this.props.fetchImages();
  }

  loadMore(page) {
    console.log(page);
    this.props.loadMore(page);
  }

  render() {

    const images = [];
    this.props.images.map(i => {
      return images.push(
          <Link
            key={`id_${i.id}`}
            to={{
              pathname: `/p/${i.id}`,
              state: { modal: true }
            }}
          >
            <Thumbnail url={i.urls.small}/>
          </Link>
        )
    })

    return (
      <div>
        <Profile />
        <InfiniteScroll
            pageStart={2}
            loadMore={this.loadMore.bind(this)}
            hasMore={true}
            loader={<Loader />}>
            
            <Layout className="layout">
              <Content className="container">
                { images }
              </Content>
            </Layout>
        </InfiniteScroll>
      </div>
    );
  }
}
Gallery.propTypes = {
  fetchImages: PropTypes.func.isRequired,
  loadMore: PropTypes.func.isRequired,
  images: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
  images: state.images.items
})

export default connect(mapStateToProps, { fetchImages, loadMore })(Gallery);
