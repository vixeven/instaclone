import React, { Component } from "react";

import './PostInfo.scss';
import logo from '../profile/logo.png'

class PostInfo extends Component {
  render() {
    return (
        <div className="postInfo">
            <div className="userdata">
                <img src={logo} className='logo' alt="logo"/>
                <div className="user">
                    <a href="https://instagram.com/ebsintegrator" className="username">ebsintegrator</a>
                    <a href="https://instagram.com/ebsintegrator" className="location">Chisinau, Moldova</a>
                </div>
            </div>
        </div>
    );
  }
}

export default PostInfo;
