import React from 'react';
import { Link } from 'react-router-dom';

import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Icon from 'antd/lib/icon'

import logo from './instagram.svg';
import icon from './instagram.png';

import './Header.scss'

class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <Row type="flex" justify="space-around" align="middle">
          <Col span={ 4 } >
            <Link to='/' className="logos">
              <img src={icon} height={'40px'} alt='icon'/>
              <div className="separator" />
              <img src={logo} height={'50px'} alt='logo'/>
            </Link>
          </Col>
          <Col span={0} md={4}>
            <input placeholder='Search'/>
          </Col>
          <Col span={4} className="icons">
            <Icon type="compass"/>
            <Icon type="heart"/>
            <Icon type="user"/>
          </Col>
        </Row>
      </div>
    );
  }
}
  
export default Header;
