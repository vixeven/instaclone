import React, { Component } from 'react';
import Image from '../image/Image';

import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { getImage } from '../../actions/imageActions';

import './ImageView.scss';
import PostInfo from '../post-info/PostInfo';
import Loader from "../loader/Loader";

class ImageView extends Component {
    async componentWillMount() {
        console.log(this.props.match.params.id);
        await this.props.getImage(this.props.match.params.id);
    }

    render() {
        if (!this.props.image)
            return <div>Image not found</div>;
        
        const link = this.props.image ? (this.props.image.urls || {}).regular || 0 : 0;
        console.log(link);

        if (!link)
            return <Loader />

        return (
        <div className="card">
            <Image url={link} />
            <PostInfo />
        </div>
        );
    }
}

ImageView.propTypes = {
    getImage: PropTypes.func.isRequired,
    image: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    image: state.images.item
})

export default connect(mapStateToProps, { getImage })(ImageView);
