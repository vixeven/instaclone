import React, { Component } from "react";

import './Thumbnail.scss';

class Thumbnail extends Component {
  render() {
    return (
        <div className="thumbnail"
        style={{
            backgroundImage: 'url(' + this.props.url + ')'
        }}
      />
    );
  }
}

export default Thumbnail;
