import React, { Component } from "react";

import './Image.scss'

class Image extends Component {
  render() {
    console.log(this.props.url);
    return (
      <div>
        <img className="image" src={this.props.url} alt={this.props.alt}/>
      </div>
    );
  }
}

export default Image;
