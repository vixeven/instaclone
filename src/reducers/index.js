import { combineReducers } from 'redux';
import imagesReducers from './imagesReducers';

export default combineReducers({
    images: imagesReducers
})