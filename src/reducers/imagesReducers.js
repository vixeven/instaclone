import { FETCH_IMAGES, LOAD_IMAGES, GET_IMAGE } from '../actions/types';

const initialState = {
    items: [],
    item: {},
}

export default function(state = initialState, actions) {
    switch(actions.type) {
        case FETCH_IMAGES:
            return {
                ...state,
                items: actions.payload
            }
        case GET_IMAGE:
            return {
                ...state,
                item: actions.payload
            }
        case LOAD_IMAGES:
            return {
                ...state,
                items: [...state.items, ...actions.payload]
            }
        default:
            return state;
    }
}