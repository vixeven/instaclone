import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import dotenv from 'dotenv';


import './index.scss';

dotenv.config()

ReactDOM.render(// In your src/index.js,
    <BrowserRouter basename={process.env.PUBLIC_URL}>
        <App />
    </BrowserRouter>
    , document.getElementById('root'));
